webpackJsonp([9],{

/***/ 270:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__bookmarks__ = __webpack_require__(419);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BookmarksPageModule", function() { return BookmarksPageModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var BookmarksPageModule = (function () {
    function BookmarksPageModule() {
    }
    return BookmarksPageModule;
}());
BookmarksPageModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__bookmarks__["a" /* BookmarksPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__bookmarks__["a" /* BookmarksPage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__bookmarks__["a" /* BookmarksPage */]
        ]
    })
], BookmarksPageModule);

//# sourceMappingURL=bookmarks.module.js.map

/***/ }),

/***/ 419:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(197);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BookmarksPage; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var BookmarksPage = (function () {
    function BookmarksPage(navCtrl, navParams, storage, toastCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.toastCtrl = toastCtrl;
        this.bookmarks = [];
        this.storage.get('bookmarks').then(function (bookmarks) {
            _this.bookmarks = bookmarks;
        });
    }
    BookmarksPage.prototype.removeBookmark = function (newsId) {
        console.log("newsId--" + newsId);
        for (var i = 0; i <= this.bookmarks.length - 1; i++) {
            if (this.bookmarks[i].id == newsId) {
                this.bookmarks.splice(i, 1);
                this.storage.set('bookmarks', this.bookmarks);
                this.createToaster('removed from bookmarks', 3000);
            }
        }
    };
    BookmarksPage.prototype.createToaster = function (message, duration) {
        var toast = this.toastCtrl.create({
            message: message,
            duration: duration
        });
        toast.present();
    };
    BookmarksPage.prototype.isBookmarks = function () {
        if (this.bookmarks.length == 0) {
            return false;
        }
        else {
            return true;
        }
    };
    BookmarksPage.prototype.newsDetails = function (key) {
        this.navCtrl.push("NewsDetailPage", {
            key: key
        });
    };
    return BookmarksPage;
}());
BookmarksPage = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-bookmarks',template:/*ion-inline-start:"/Users/jessevangemeren/Desktop/i3_news/i3_local/src/pages/bookmarks/bookmarks.html"*/'<ion-header>\n  <ion-navbar hideBackButton="true">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Slim News</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content [ngClass]="{bg:!isBookmarks()}">\n  <div *ngIf="!isBookmarks()">\n    <h4>Bookmark List is empty!</h4>\n  </div>\n\n  <ion-card class="category" *ngIf="isBookmarks()">\n    <div  *ngFor="let bookmark of bookmarks">\n      <div (click)="newsDetails(bookmark.id)">\n        <div class="image">\n          <img src={{bookmark.thumb}}>\n          <div class="bg-overlay"></div>\n        </div>\n      </div>\n      <ion-card-content >\n        <ion-row>\n          <ion-col col-11>\n            <p>{{bookmark.title}}</p>\n          </ion-col>\n          <ion-col col-1>\n            <ion-icon (click)="removeBookmark(bookmark.id)" class="bookmark" name="trash"></ion-icon>\n          </ion-col>\n        </ion-row>\n      </ion-card-content>\n    </div>\n  </ion-card>\n</ion-content>\n'/*ion-inline-end:"/Users/jessevangemeren/Desktop/i3_news/i3_local/src/pages/bookmarks/bookmarks.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ToastController */]])
], BookmarksPage);

//# sourceMappingURL=bookmarks.js.map

/***/ })

});
//# sourceMappingURL=9.main.js.map