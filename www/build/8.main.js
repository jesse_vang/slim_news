webpackJsonp([8],{

/***/ 271:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__category__ = __webpack_require__(420);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategoryPageModule", function() { return CategoryPageModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var CategoryPageModule = (function () {
    function CategoryPageModule() {
    }
    return CategoryPageModule;
}());
CategoryPageModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__category__["a" /* CategoryPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__category__["a" /* CategoryPage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__category__["a" /* CategoryPage */]
        ]
    })
], CategoryPageModule);

//# sourceMappingURL=category.module.js.map

/***/ }),

/***/ 420:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_news_service__ = __webpack_require__(195);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CategoryPage; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CategoryPage = (function () {
    function CategoryPage(navCtrl, navParams, newsService, toastCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.newsService = newsService;
        this.toastCtrl = toastCtrl;
        this.checkVisible = false;
        this.categorItems = [];
        this.checkedItem = [];
        this.newsService.getData()
            .subscribe(function (res) {
            _this.categorItems = res.categories;
        });
    }
    CategoryPage.prototype.checkBoxToggle = function (item) {
        item.checkVisible = !item.checkVisible;
        this.key = item.id;
        if (item.checkVisible == true) {
            this.checkedItem.push(this.key);
        }
        else {
            var index = this.checkedItem.indexOf(this.key);
            this.checkedItem.splice(index, 1);
        }
    };
    CategoryPage.prototype.createToaster = function (message, duration) {
        var toast = this.toastCtrl.create({
            message: message,
            duration: duration
        });
        toast.present();
    };
    CategoryPage.prototype.home = function () {
        if (this.checkedItem.length <= 0) {
            //console.log("Plz select any category");
            this.createToaster("please select atleast one category", 3000);
        }
        else {
            localStorage.setItem('categories', JSON.stringify(this.checkedItem));
            this.navCtrl.push("HomePage");
        }
    };
    return CategoryPage;
}());
CategoryPage = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-category',template:/*ion-inline-start:"/Users/jessevangemeren/Desktop/i3_news/i3_local/src/pages/category/category.html"*/'<ion-header>\n  <ion-navbar hideBackButton="true">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Slim News</ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content class="category-content">\n  <div class="overlay-bg"></div>\n  <div class="title">\n    <p>SELECT YOUR</p>\n    <P><span>FAVOURITE </span>CATEGORIES</P>\n\n  </div>\n\n  <ion-row class="category-grid">\n    <ion-col col-6 class="grid-item-1" (click)="checkBoxToggle(item)" *ngFor="let item of categorItems"\n             [style.background]="item.color">\n      <div *ngIf="item.icon !=\'\' ">\n        <ion-icon class="img-icon" name="{{item.icon}}"></ion-icon>\n      </div>\n      <div *ngIf="item.icon==\'\' ">\n        <ion-icon class="img-icon" name="images"></ion-icon>\n      </div>\n      <h4>{{item.title}}</h4>\n      <div class="checkBox " *ngIf="item.checkVisible">\n        <ion-icon name="checkmark" class="check"></ion-icon>\n      </div>\n    </ion-col>\n  </ion-row>\n\n  <div class="btn-wrapper">\n    <button ion-button block class="confirm-btn" (click)="home()">CONFIRM</button>\n  </div>\n\n</ion-content>\n'/*ion-inline-end:"/Users/jessevangemeren/Desktop/i3_news/i3_local/src/pages/category/category.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_news_service__["a" /* NewsService */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ToastController */]])
], CategoryPage);

//# sourceMappingURL=category.js.map

/***/ })

});
//# sourceMappingURL=8.main.js.map