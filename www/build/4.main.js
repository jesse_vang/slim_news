webpackJsonp([4],{

/***/ 278:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__registration__ = __webpack_require__(427);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegistrationPageModule", function() { return RegistrationPageModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var RegistrationPageModule = (function () {
    function RegistrationPageModule() {
    }
    return RegistrationPageModule;
}());
RegistrationPageModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__registration__["a" /* RegistrationPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__registration__["a" /* RegistrationPage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__registration__["a" /* RegistrationPage */]
        ]
    })
], RegistrationPageModule);

//# sourceMappingURL=registration.module.js.map

/***/ }),

/***/ 427:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(13);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegistrationPage; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var RegistrationPage = (function () {
    function RegistrationPage(navCtrl, fb) {
        this.navCtrl = navCtrl;
        this.fb = fb;
        this.user = {
            fullName: '',
            email: '',
            password: '',
            mobileNumber: '',
            role: 'user'
        };
    }
    RegistrationPage.prototype.ngOnInit = function () {
        this.registration = this.fb.group({
            'fullName': new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["e" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required),
            'email': new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["e" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required),
            'password': new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["e" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required),
            'mobileNumber': new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["e" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required)
        });
    };
    RegistrationPage.prototype.onRegistration = function () {
        console.log("user" + JSON.stringify(this.registration.value));
        localStorage.setItem('uid', this.registration.value.email);
        this.navCtrl.push("CategoryPage");
    };
    RegistrationPage.prototype.navLogin = function () {
        this.navCtrl.push("LoginPage");
    };
    return RegistrationPage;
}());
RegistrationPage = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-registration',template:/*ion-inline-start:"/Users/jessevangemeren/Desktop/i3_news/i3_local/src/pages/registration/registration.html"*/'<ion-content class="register-content">\n  <div class="overlay-bg"></div>\n  <div class="header">\n    <img src="assets/icon/logoIcon.png">\n    <p class="title">Slim News</p>\n  </div>\n  <form [formGroup]="registration" (ngSubmit)="onRegistration()">\n    <div class="register-wrapper">\n      <ion-item class="input-item">\n        <ion-input type="text" placeholder="Full Name" formControlName="fullName"></ion-input>\n        <ion-icon ios="ios-person-outline" md="md-person" item-right></ion-icon>\n      </ion-item>\n      <ion-item class="input-item">\n        <ion-input type="email" placeholder="Email" formControlName="email"></ion-input>\n        <ion-icon ios="ios-mail-outline" md="md-mail" item-right></ion-icon>\n      </ion-item>\n      <ion-item class="input-item">\n        <ion-input type="password" placeholder="Password" formControlName="password"></ion-input>\n        <ion-icon ios="ios-lock-outline" md="md-lock" item-right></ion-icon>\n      </ion-item>\n\n      <ion-item class="input-item">\n        <ion-input type="number" placeholder="Mobile Number" formControlName="mobileNumber"></ion-input>\n        <ion-icon ios="ios-call-outline" md="md-call" item-right></ion-icon>\n      </ion-item>\n\n      <button ion-button block class="register-btn" type="submit"\n        [disabled]=!registration.valid>REGISTER</button>\n\n      <p class="link">Already Have An Account? <a (click)="navLogin()">Login</a></p>\n    </div>\n  </form>\n</ion-content>\n'/*ion-inline-end:"/Users/jessevangemeren/Desktop/i3_news/i3_local/src/pages/registration/registration.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* FormBuilder */]])
], RegistrationPage);

//# sourceMappingURL=registration.js.map

/***/ })

});
//# sourceMappingURL=4.main.js.map