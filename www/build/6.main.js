webpackJsonp([6],{

/***/ 274:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__landing_page__ = __webpack_require__(423);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LandingPagePageModule", function() { return LandingPagePageModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var LandingPagePageModule = (function () {
    function LandingPagePageModule() {
    }
    return LandingPagePageModule;
}());
LandingPagePageModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__landing_page__["a" /* LandingPagePage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__landing_page__["a" /* LandingPagePage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__landing_page__["a" /* LandingPagePage */]
        ]
    })
], LandingPagePageModule);

//# sourceMappingURL=landing-page.module.js.map

/***/ }),

/***/ 423:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(100);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LandingPagePage; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var LandingPagePage = (function () {
    function LandingPagePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    LandingPagePage.prototype.navLogin = function () {
        this.navCtrl.push("LoginPage");
    };
    LandingPagePage.prototype.navSignin = function () {
        this.navCtrl.push("RegistrationPage");
    };
    LandingPagePage.prototype.home = function () {
        this.navCtrl.push("HomePage");
    };
    return LandingPagePage;
}());
LandingPagePage = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-landing-page',template:/*ion-inline-start:"/Users/jessevangemeren/Desktop/i3_news/i3_local/src/pages/landing-page/landing-page.html"*/'<ion-content class="page-content">\n	<div class="overlay-bg"></div>\n    <div class="header">\n        <img src="assets/icon/logoIcon.png">\n        <p class="title">Slim News</p>\n    </div>\n    <div class="button-grp">\n        <button ion-button block class="signin-btn" (click)="navLogin()">Sign In</button>\n        <button ion-button block class="signup-btn" (click)="navSignin()">Sign Up</button>\n        <p (click)="home()">Skip this step</p>\n    </div>\n</ion-content>\n'/*ion-inline-end:"/Users/jessevangemeren/Desktop/i3_news/i3_local/src/pages/landing-page/landing-page.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
], LandingPagePage);

//# sourceMappingURL=landing-page.js.map

/***/ })

});
//# sourceMappingURL=6.main.js.map