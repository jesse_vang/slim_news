import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { MyApp } from './app.component';
import { ReactiveFormsModule } from '@angular/forms';
import { NewsService } from '../providers/news-service';
import { Geolocation } from '@ionic-native/geolocation';
import { WeatherProvider } from '../providers/weather/weather';
import { IonicStorageModule } from '@ionic/storage';

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    IonicModule.forRoot(MyApp),
    ReactiveFormsModule,
    IonicStorageModule.forRoot(),
    BrowserModule,
    HttpModule
  ],
  bootstrap: [ IonicApp ],
  entryComponents: [
    MyApp
  ],
  providers: [ { provide: ErrorHandler, useClass: IonicErrorHandler }, NewsService, Geolocation, WeatherProvider ]
})
export class AppModule {
}
