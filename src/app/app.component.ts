import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { OneSignal } from '@ionic-native/onesignal';
import { SocialSharing } from '@ionic-native/social-sharing';

@Component({
  templateUrl: 'app.html',
  selector: 'MyApp',
  providers: [ OneSignal, SocialSharing, SplashScreen, StatusBar ]
})

export class MyApp {
  @ViewChild(Nav) nav: Nav;
  user: any = {};
  rootPage: string;
  url: any;
  uid: any;
  pages: Array<{ title: string, component: any }>;

  constructor(public platform: Platform,
              public oneSignal: OneSignal,
              public socialSharing: SocialSharing,
              public statusBar: StatusBar,
              public splashScreen: SplashScreen) {
    this.initializeApp();
  }

  ngOnInit() {
    if (localStorage.getItem('uid') != null) {
      this.rootPage = "LandingPagePage";
    }
    else {
      this.rootPage = "HomePage";
    }
    this.url = "assets/img/profile.jpg";
  }

  initializeApp() {
    this.platform.ready().then((res) => {
      if (res == 'cordova') {
        this.oneSignal.startInit('44022861-6ffe-4dad-9690-8aa7d8276c1b', '867480660537');
        this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);
        this.oneSignal.handleNotificationReceived().subscribe(() => {
        });
        this.oneSignal.handleNotificationOpened().subscribe((res) => {
        });
        this.oneSignal.endInit();
      }
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    this.nav.setRoot(page.component);
  }

  home() {
    this.nav.setRoot("HomePage");
  }

  category() {
    this.nav.setRoot("CategoryPage");
  }

  bookmarks() {
    this.nav.setRoot("BookmarksPage");
  }

  setting() {
    this.nav.setRoot("SettingPage");
  }

  profile() {
    this.nav.setRoot("ProfilePage");
  }

  login() {
    this.nav.setRoot("LoginPage");
  }

  logout() {
    localStorage.removeItem('uid');
    this.nav.setRoot("LandingPagePage");
  }

  isLoggedin() {
    return localStorage.getItem('uid') != null;
  }
}

