import {Component, OnInit} from '@angular/core';
import {NavController, IonicPage} from 'ionic-angular';
import {FormGroup, FormControl,FormBuilder, Validators} from "@angular/forms";

@IonicPage()
@Component({
  selector: 'page-registration',
  templateUrl: 'registration.html'
})
export class RegistrationPage implements OnInit {
  registration: FormGroup;
  user: any = {
    fullName: '',
    email: '',
    password: '',
    mobileNumber: '',
    role: 'user'
  };
  date: any;

  constructor(public navCtrl: NavController,public fb:FormBuilder) {
  }

  ngOnInit() {
    this.registration = this.fb.group({
      'fullName': new FormControl('', Validators.required),
      'email': new FormControl('', Validators.required),
      'password': new FormControl('', Validators.required),
      'mobileNumber': new FormControl('', Validators.required)
    });
  }

  onRegistration() {
        console.log("user"+JSON.stringify(this.registration.value));
        localStorage.setItem('uid',this.registration.value.email);
        this.navCtrl.push("CategoryPage");
      }

  navLogin() {
    this.navCtrl.push("LoginPage");
  }
}
