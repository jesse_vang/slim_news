import { Component, OnInit } from '@angular/core';
import { NavController, IonicPage, Platform, LoadingController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Facebook } from "ng2-cordova-oauth/core";
import { OauthCordova } from 'ng2-cordova-oauth/platform/cordova';
import { TwitterConnect } from '@ionic-native/twitter-connect';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  providers: [ TwitterConnect ]
})
export class LoginPage implements OnInit {

  private oauth: OauthCordova = new OauthCordova();
  private facebookProvider: Facebook = new Facebook({
    clientId: "",
    appScope: [ "email" ]
  });

  user: FormGroup;

  constructor(public navCtrl: NavController,
              public platform: Platform,
              public fb: FormBuilder,
              public twitter: TwitterConnect,
              public loadingCtrl: LoadingController) {
  }

  ngOnInit(): any {
    this.user = this.fb.group({
      email: [ 'jessev@ontariodev.com', Validators.required ],
      password: [ '123123', Validators.required ],

    });
  }

  onLogin() {
    console.log("user" + JSON.stringify(this.user.value));
    localStorage.setItem('uid', this.user.value.email);
    this.navCtrl.push("HomePage");

  }

  gotophoneNumberPage() {
    this.navCtrl.push("SignInWithPhonenumberPage");
  }

  twitterLogin() {
    this.platform.ready().then((res) => {
        if (res == 'cordova') {
          let loading = this.loadingCtrl.create({
            content: 'Login Please Wait...'
          });
          loading.present();
          this.twitter.login().then((result) => {
            console.log("twitter res--" + JSON.stringify(result));

            this.twitter.showUser().then((user) => {
                console.log("user--" + JSON.stringify(user));
                localStorage.setItem('uid', result.token);
                loading.dismiss();
                this.navCtrl.push("HomePage");

              },
              (onError) => {
                console.log("user--" + JSON.stringify(onError));
              }
            )
          })
        }
      }
    )
  }

  onFacebookLogin() {
    this.oauth.logInVia(this.facebookProvider).then((success) => {
      console.log("RESULT: " + JSON.stringify(success));
      let res: any = success;
      console.log("token-" + JSON.stringify(res.access_token));
      localStorage.setItem('uid', res.access_token);
      this.navCtrl.push("HomePage");
    }, error => {
      console.log("FACEBOOK_ERROR: ", error);
    });
  }

  navRegister() {
    this.navCtrl.push("RegistrationPage");
  }
}
