import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-bookmarks',
  templateUrl: 'bookmarks.html',
})
export class BookmarksPage {
  bookmarks: any[] = [];

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public storage: Storage,
              public toastCtrl: ToastController) {
    this.storage.get('bookmarks').then((bookmarks) => {
      this.bookmarks = bookmarks;
    })
  }

  removeBookmark(newsId) {
    console.log("newsId--" + newsId);
    for (let i = 0; i <= this.bookmarks.length - 1; i++) {
      if (this.bookmarks[ i ].id == newsId) {
        this.bookmarks.splice(i, 1);
        this.storage.set('bookmarks', this.bookmarks);
        this.createToaster('removed from bookmarks', 3000);
      }
    }

  }

  createToaster(message, duration) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: duration
    });
    toast.present();
  }

  isBookmarks(): boolean {
    if (this.bookmarks.length == 0) {
      return false;
    }
    else {
      return true;
    }
  }


  newsDetails(key) {
    this.navCtrl.push("NewsDetailPage", {
      key: key
    })
  }

}
