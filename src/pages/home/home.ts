import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage, LoadingController } from 'ionic-angular';
import { NewsService } from '../../providers/news-service';
import { WeatherProvider } from '../../providers/weather/weather';
import { SocialSharing } from '@ionic-native/social-sharing';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  state = "normal";
  searchBarVisible = false;
  keys: any[] = [];
  totalNewsArray: any[] = [];
  newsArray: any[] = [];
  sevenDaysBack: any;
  weatherList: any[] = [ {
    city: { country: '' },
    list: [ {
      temp: {
        day: ''
      }
    } ]
  } ];
  showWeather: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public loadingCtrl: LoadingController,
              public newsService: NewsService,
              public weather: WeatherProvider,
              public socialSharing: SocialSharing) {
    this.showWeather = localStorage.getItem('showWeather');
    this.weather.getLocalWeather().subscribe(
      data => {
        this.weatherList = data;
      })
  }

  ngOnInit() {
    let date = new Date();
    let midnight = date.setUTCHours(0, 0, 0, 0);
    this.sevenDaysBack = midnight - 7 * 24 * 60 * 60 * 1000;
    this.keys = JSON.parse(localStorage.getItem('categories'));
    let loader = this.loadingCtrl.create({
      content: 'Loading Please Wait...'
    });
    loader.present().then(() => {
      this.newsService.getData()
        .subscribe(res => {
          this.totalNewsArray = res.news;
          loader.dismiss();
          if (this.keys != null) {
            let total = this.totalNewsArray.length - 1;
            for (let i = 0; i <= this.keys.length - 1; i++) {
              for (let j = total; j >= 0; j--) {
                if (this.totalNewsArray[ j ].categoryId == this.keys[ i ]) {
                  this.newsArray.push(this.totalNewsArray[ j ]);
                }
              }
            }
            localStorage.removeItem('categories');
          }
          else {
            let total = this.totalNewsArray.length - 1;
            for (let i = total; i >= 0; i--) {
              this.newsArray.push(this.totalNewsArray[ i ]);
            }
          }

        })

    })
  }

  share(news) {
    this.socialSharing.share(news.description, null, news.thumb, null);
  }

  searchToggle() {
    this.searchBarVisible = !this.searchBarVisible;
  }

  newsDetail(trending) {
    let key = trending.id;
    this.navCtrl.push("NewsDetailPage", { key });
  }
}

