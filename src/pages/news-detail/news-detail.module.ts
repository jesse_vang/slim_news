import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewsDetailPage } from './news-detail';
import { MomentModule } from 'angular2-moment';

@NgModule({
  declarations: [
    NewsDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(NewsDetailPage),
    MomentModule
  ],
  exports: [
    NewsDetailPage
  ]
})
export class NewsDetailPageModule {}
