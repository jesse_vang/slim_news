import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage, ToastController } from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';
import { NewsService } from '../../providers/news-service';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-news-detail',
  templateUrl: 'news-detail.html',
  providers: [ SocialSharing ]
})
export class NewsDetailPage {

  searchBarVisible = false;
  key: string;
  article: any = {};
  user: any = {
    comment: ''
  };

  comments: any[] = [];
  dateTime: any;
  sevenDaysBack: any;
  uid: any;
  bookmarks: any[] = [];
  bookmark: any[] = [];
  bookmarkItems: any[] = [];
  news: any = {};

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public toastCtrl: ToastController,
              public socialSharing: SocialSharing,
              public newsService: NewsService,
              public storage: Storage) {

    this.key = this.navParams.get('key');
    this.newsService.getData()
      .subscribe(res => {
        for (let i = 0; i <= res.news.length - 1; i++) {
          if (res.news[ i ].id == this.key) {
            this.article = res.news[ i ];
            this.news.id = this.article.id;
            this.news.title = this.article.title;
            this.news.thumb = this.article.thumb;
          }
        }
      })
  }

  ngOnInit() {
    let date = new Date();
    let midnight = date.setUTCHours(0, 0, 0, 0);
    this.sevenDaysBack = midnight - 7 * 24 * 60 * 60 * 1000;
    this.uid = localStorage.getItem('uid');
    this.storage.get('bookmarks').then((bookmarks) => {
      this.bookmarks = bookmarks;
    })
  }

  visible = true;

  toggleFavourite() {
    this.visible = !this.visible;
  }

  addBookmark(key) {
    if (this.bookmarks == null) {
      this.bookmark.push(this.news);
      this.storage.set('bookmarks', this.bookmark);
    }
    else {
      for (let i = 0; i <= this.bookmarks.length - 1; i++) {
        if (this.bookmarks[ i ].id == key) {
          this.bookmarks.splice(i, 1);
        }
      }
      this.bookmarks.push(this.news);
      this.storage.set('bookmarks', this.bookmarks);
      this.createToaster('Added to bookmarks', '3000');
    }
  }

  removeBookmark(key) {
    this.storage.get('bookmarks').then((bookmarks) => {
      this.bookmarkItems = bookmarks;
      for (let i = 0; i <= this.bookmarkItems.length - 1; i++) {
        if (this.bookmarkItems[ i ].id == key) {
          this.bookmarkItems.splice(i, 1);
        }
      }
      this.storage.set('bookmarks', this.bookmarkItems);
      this.createToaster('Removed from bookmarks', '3000');
    })
  }

  createToaster(message, duration) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: duration
    });
    toast.present();
  }

  searchToggle() {
    this.searchBarVisible = !this.searchBarVisible;
  }

  fbSharing() {
    this.socialSharing.shareViaFacebook(this.article.description, null, null);
  }

  twitterSharing() {
    this.socialSharing.shareViaTwitter(this.article.description, null, null);
  }

  googleSharing() {
    this.socialSharing.share(this.article.description, null, null, null);
  }

  whatsAppSharing() {
    this.socialSharing.shareViaWhatsApp(this.article.description, null, null);
  }
}
