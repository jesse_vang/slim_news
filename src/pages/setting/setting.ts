import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-setting',
  templateUrl: 'setting.html',
})
export class SettingPage {
  showWeather:any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    this.showWeather=localStorage.getItem('showWeather');
    console.log('--'+this.showWeather);
  }

  changeSetting(){
    console.log('--'+this.showWeather);
    localStorage.setItem('showWeather',this.showWeather)
  }
}
