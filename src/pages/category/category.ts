import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage, ToastController } from 'ionic-angular';
import { NewsService } from '../../providers/news-service';

@IonicPage()
@Component({
  selector: 'page-category',
  templateUrl: 'category.html'
})
export class CategoryPage {
  checkVisible = false;
  categorItems: any[] = [];
  key: string;
  checkedItem: any[] = [];

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public newsService: NewsService,
              public toastCtrl: ToastController) {
    this.newsService.getData()
      .subscribe(res => {
        this.categorItems = res.categories;
      })
  }

  checkBoxToggle(item) {
    item.checkVisible = !item.checkVisible;
    this.key = item.id;
    if (item.checkVisible == true) {

      this.checkedItem.push(this.key);

    } else {
      let index = this.checkedItem.indexOf(this.key);
      this.checkedItem.splice(index, 1);

    }
  }

  createToaster(message, duration) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: duration
    });
    toast.present();
  }

  home() {
    if (this.checkedItem.length <= 0) {
      //console.log("Plz select any category");
      this.createToaster("please select atleast one category", 3000);
    } else {
      localStorage.setItem('categories', JSON.stringify(this.checkedItem));
      this.navCtrl.push("HomePage");
    }

  }

}
