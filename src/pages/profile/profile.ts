import { Component } from '@angular/core';
import { NavController, IonicPage } from 'ionic-angular';
import { NgForm } from "@angular/forms";

@IonicPage()

@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})

export class ProfilePage {

  user: any = {};
  url: any;
  key: any;

  constructor(public navCtrl: NavController) {
    this.url = "assets/img/profile.jpg";
  }

  onSubmit(user: NgForm) {

  }

  readUrl(event) {
    if (event.target.files && event.target.files[ 0 ]) {
      var reader = new FileReader();
      reader.onload = (event: any) => {
        this.url = event.target.result;
      };
      reader.readAsDataURL(event.target.files[ 0 ]);
    }
  }
}
