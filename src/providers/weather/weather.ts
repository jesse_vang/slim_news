import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Geolocation } from '@ionic-native/geolocation';

@Injectable()
export class WeatherProvider {
  lat: number;
  lng: number;
  private apiKey = '58fa8ab6728c87b51c8bd90e4ca46d46';
  private baseUrl = 'http://api.openweathermap.org/data/2.5/';

  constructor(public http: Http, private geolocation: Geolocation) {
  }

  getLocalWeather() {
    let Obs = Observable.create(observer => {
      this.geolocation.getCurrentPosition().then((resp) => {
        this.lat = resp.coords.latitude;
        this.lng = resp.coords.longitude;
        let url = this.baseUrl + 'forecast/daily';
        url += `?lat=${this.lat}&lon=${this.lng}&units=metric&dt=UTC`;
        url += '&appid=' + this.apiKey;
        return this.http.get(url)
          .subscribe(data => {
              observer.next(data.json());
            },
            err => observer.error(err),
            () => observer.compelte
          )
      }).catch((error) => {
        console.log('Error getting location', error);
      })
    });

    return Obs;
  }
}
